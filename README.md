# Advent of Code 2023

My solutions for Advent of Code 2023, implemented in Common Lisp.

See https://adventofcode.com/2023/about to learn about Advent of Code.

## Author

* Kevin Wayne Smith

## Copyright

Copyright (c) 2023 Kevin Wayne Smith

## License

Licensed under the MIT License.
