(in-package :aoc2023)

(format t "~&~A~%" (str:repeat 80 "*"))

(format t "~&Solution to AoC 2023 Day 1 Part 1: ~A~%" (day1:solution-part1))
(format t "~&Solution to AoC 2023 Day 1 Part 2: ~A~%" (day1:solution-part2))

(format t "~2&Solution to AoC 2023 Day 2 Part 1: ~A~%" (day2:solution-part1))
(format t "~&Solution to AoC 2023 Day 2 Part 2: ~A~%" (day2:solution-part2))

(format t "~2&Solution to AoC 2023 Day 3 Part 1: ~A~%" (day3:solution-part1))
(format t "~&Solution to AoC 2023 Day 3 Part 2: ~A~%" (day3:solution-part2))

(format t "~2&Solution to AoC 2023 Day 4 Part 1: ~A~%" (day4:solution-part1))
(format t "~&Solution to AoC 2023 Day 4 Part 2: ~A~%" (day4:solution-part2))

(format t "~2&Solution to AoC 2023 Day 5 Part 1: ~A~%" (day5:solution-part1))
(format t "~&Solution to AoC 2023 Day 5 Part 2: ~A~%" (day5:solution-part2))

(format t "~2&Solution to AoC 2023 Day 6 Part 1: ~A~%" (day6:solution-part1))
(format t "~&Solution to AoC 2023 Day 6 Part 2: ~A~%" (day6:solution-part2))

(format t "~2&Solution to AoC 2023 Day 7 Part 1: ~A~%" (day7:solution-part1))
(format t "~&Solution to AoC 2023 Day 7 Part 2: ~A~%" (day7:solution-part2))

(format t "~2&Solution to AoC 2023 Day 8 Part 1: ~A~%" (day8:solution-part1))
(format t "~&Solution to AoC 2023 Day 8 Part 2: ~A~%" (day8:solution-part2))

(format t "~2&Solution to AoC 2023 Day 9 Part 1: ~A~%" (day9:solution-part1))
(format t "~&Solution to AoC 2023 Day 9 Part 2: ~A~%" (day9:solution-part2))

(format t "~2&Solution to AoC 2023 Day 10 Part 1: ~A~%" (day10:solution-part1))
(format t "~&Solution to AoC 2023 Day 10 Part 2: ~A~%" (day10:solution-part2))

(format t "~2&Solution to AoC 2023 Day 11 Part 1: ~A~%" (day11:solution-part1))
(format t "~&Solution to AoC 2023 Day 11 Part 2: ~A~%" (day11:solution-part2))

(format t "~2&Solution to AoC 2023 Day 12 Part 1: ~A~%" (day12:solution-part1))
(format t "~&Solution to AoC 2023 Day 12 Part 2: ~A~%" (day12:solution-part2))

(format t "~&~A~%" (str:repeat 80 "*"))
