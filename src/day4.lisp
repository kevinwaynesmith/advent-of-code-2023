(in-package :day4)

(defstruct scratchcard
  (id 0)
  (winning-numbers nil)
  (card-numbers nil))

(defun read-scratchcards ()
  "Read in the input rows as a list of strings."
  (let* ((directory (uiop:pathname-parent-directory-pathname (uiop:getcwd)))
	 (filepath (merge-pathnames #p"data/day4/input" directory)))
    (with-open-file (in filepath :direction :input)
      (loop for line = (read-line in nil nil)
	    while line
	    collect line))))

(defun lex-scratchcard (line)
  (let* ((tokens-pattern "[0-9]+|\\|")
	 (string-tokens (cl-ppcre:all-matches-as-strings tokens-pattern line)))
    (append (mapcar (lambda (x)
		      (trivia:match x
			("|" 'BAR)
			(_ (parse-integer x))))
		    string-tokens)
	    (list 'EOF))))

(defparameter *tokens* nil)

;;; recursive-descent parser
(defun parse-scratchcard-start ()
  (let ((scratchcard (parse-scratchcard))
	(next (pop *tokens*)))
    (if (eq next 'EOF)
	scratchcard
	(error "Unexpected token: expected EOF, got ~A~%" next))))

(defun parse-scratchcard ()
  (let ((id (parse-scratchcard-number))
	(winning-numbers (parse-winning-numbers))
	(next (pop *tokens*)))
    (if (eq next 'BAR)
	(let ((card-numbers (parse-card-numbers)))
	  (make-scratchcard :id id
			    :winning-numbers winning-numbers
			    :card-numbers card-numbers))
	(error "Unexpected token: expected BAR, got ~A~%" next))))

(defun parse-winning-numbers ()
  (if (eq (car *tokens*) 'BAR)
      nil
      (cons (parse-scratchcard-number) (parse-winning-numbers))))

(defun parse-scratchcard-number ()
  (let ((next (pop *tokens*)))
    (if (numberp next)
	next
	(error "Unexpected token: expected number, got ~A~%" next))))

(defun parse-card-numbers ()
  (if (eq (car *tokens*) 'EOF)
      nil
      (cons (parse-scratchcard-number) (parse-card-numbers))))
;;; end parser

(defun get-scratchcards ()
  (mapcar (lambda (line)
	    (progn
	      (defparameter *tokens* (lex-scratchcard line))
	      (parse-scratchcard-start)))
	  (read-scratchcards)))

(defun score-scratchcard (card)
  (let ((win-count (length (intersection (scratchcard-card-numbers card)
					 (scratchcard-winning-numbers card)))))
    (if (zerop win-count)
	0
	(expt 2 (1- win-count)))))

(defun solution-part1 ()
  (reduce #'+ (mapcar #'score-scratchcard (get-scratchcards))))

;;; part 2

(defun win-count (card)
  (length (intersection (scratchcard-card-numbers card)
			(scratchcard-winning-numbers card))))

(defparameter *scratchcard-scores*
  (let ((scratchcards (get-scratchcards)))
    (make-array (list (1+ (length scratchcards)))
		:initial-contents (cons 'space-holder
					(mapcar #'win-count (get-scratchcards))))))

(defun solution-part2-slow ()
  (do* ((scratchcard-ids (mapcar #'scratchcard-id (get-scratchcards)))
	(current-id (pop scratchcard-ids) (pop scratchcard-ids))
	(count 0 (1+ count)))
       ((null current-id) count)
    (loop for i
	    downfrom (+ current-id (aref *scratchcard-scores* current-id))
	      to (1+ current-id)
	  do (push i scratchcard-ids))))

(defun solution-part2 ()
  (let ((scratchcard-counts (make-array (length *scratchcard-scores*)
					:initial-element 1)))
    (loop for i from 1 to (1- (length *scratchcard-scores*)) do
      (loop for j from (1+ i) to (+ i (aref *scratchcard-scores* i)) do
	(setf (aref scratchcard-counts j)
	      (+ (aref scratchcard-counts j) (aref scratchcard-counts i)))))
    (1- (reduce #'+ scratchcard-counts)))) ; 1- because of the 1 at index 0
