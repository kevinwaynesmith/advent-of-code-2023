(in-package :day6)

(defstruct race
  (time nil)
  (distance nil))

(defun read-input ()
  "Get the contents of the input as a string."
  (let ((filepath (merge-pathnames
		   #p"data/day6/input"
		   (uiop:pathname-parent-directory-pathname (uiop:getcwd)))))
    (with-open-file (file filepath :direction :input)
      (uiop:read-file-string file))))

(defun lex-input (string)
  "Convert string into tokens."
  (let ((string-tokens (cl-ppcre:all-matches-as-strings "[0-9]+|Time|Distance"
							string)))
    (append
     (mapcar (lambda (x)
	       (trivia:match x
		 ("Time" 'time)
		 ("Distance" 'distance)
		 (otherwise (parse-integer x))))
	     string-tokens)
     (list 'eof))))

(defparameter *tokens* nil)

(defparameter *races* nil)

;;; begin recursive-descent parser

(defun parse-start ()
  (let ((times (parse-times))
	(distances (parse-distances))
	(next (pop *tokens*)))
    (if (eq next 'eof)
	(mapcar (lambda (x y)
		  (make-race :time x :distance y))
		times
		distances)
	(error "Unexpected token: expected 'eof, got ~A~%" next))))

(defun parse-times ()
  (let ((next (pop *tokens*)))
    (if (eq next 'time)
	(parse-times-list)
	(error "Unexpected token: expected 'time, got ~A~%" next))))

(defun parse-times-list ()
  (let ((next (car *tokens*)))
    (if (eq next 'distance)
	nil
	(cons (parse-num) (parse-times-list)))))

(defun parse-distances ()
  (let ((next (pop *tokens*)))
    (if (eq next 'distance)
	(parse-distance-list)
	(error "Unexpected token: expected 'distance, got ~A~%" next))))

(defun parse-distance-list ()
  (let ((next (car *tokens*)))
    (if (eq next 'eof)
	nil
	(cons (parse-num) (parse-distance-list)))))

(defun parse-num ()
  (let ((num (pop *tokens*)))
    (if (numberp num)
	num
	(error "Unexpected token: expected number, got ~A~%" num))))

;;; end recursive-descent parser

;; distance = (timeTotal - holdTime) * holdTime, so
;; d = (t - h) * h, so
;; d = th - h^c, so
;; d = -h^2 + th
;; d must be greater than the current record r, so
;; -h^2 + th > r
;; -h^2 + th - r > 0
;; To tie the record, -h^2 + th - r = 0
;; This is a quadratic equation, i.e., it has the form ax^2 + bx + c = 0
;; We solve the quadratic equation to find the roots, then use these roots
;; to calculate the maximum and minimum integer hold times that will exceed
;; the record, and get the number of possibilites from maximum - minimum + 1.
(defun ways-to-win (race)
  "Return count of hold-times that will win the race."
  (let* ((time (coerce (race-time race)
		       'double-float)) ; single-point fp causes failure in part 2
	 (distance (race-distance race))
	 (rhs (sqrt (- (* time time)
		       (* 4 distance))))
	 (root-1 (/ (- time rhs) 2))
	 (root-2 (/ (+ time rhs) 2)))
    (1+ (- (1- (ceiling root-2))
	   (1+ (floor root-1))))))

(defun solution-part1 ()
  "Return solution for part 1."
  (defparameter *tokens* (lex-input (read-input)))
  (reduce #'* (mapcar #'ways-to-win (parse-start))))

;;; part2

(defun lex-input-part2 (string)
  "Convert string to tokens (consistent with part 2's reading of the input)."
  (let ((string-tokens (cl-ppcre:all-matches-as-strings "Time|Distance|([0-9]+\\s*)+"
							string)))
    (append
     (mapcar (lambda (x)
	       (trivia:match x
		 ("Time" 'time)
		 ("Distance" 'distance)
		 (_ (parse-integer (apply #'str:concat (str:words x))))))
	     string-tokens)
     (list 'eof))))

(defun parse-start-part2 ()
  (let ((time (parse-time-part2))
	(distance (parse-distance-part2))
	(next (pop *tokens*)))
    (if (eq next 'eof)
	(make-race :time time :distance distance)
	(error "Unexpected token: expected 'eof, got ~A~%" next))))

(defun parse-time-part2 ()
  (let ((next (pop *tokens*)))
    (if (eq next 'time)
	(parse-num-part2)
	(error "Unexpected token: expected 'time, got ~A~%" next))))

(defun parse-distance-part2 ()
  (let ((next (pop *tokens*)))
    (if (eq next 'distance)
	(parse-num-part2)
	(error "Unexpected token: expected 'distance, got ~A~%" next))))

(defun parse-num-part2 ()
  (let ((num (pop *tokens*)))
    (if (numberp num)
	num
	(error "Unexpected token: expected number, got ~A~%" num))))

(defun solution-part2 ()
  "Return solution to part 2."
  (defparameter *tokens* (lex-input-part2 (read-input)))
  (ways-to-win (parse-start-part2)))
