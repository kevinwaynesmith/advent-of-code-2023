(in-package :day2)

(defstruct cube-game
  (id 0)
  (sets nil))

(defstruct cube-set
  (reds 0)
  (greens 0)
  (blues 0))


(defun lex-cube-game (line)
  "Lexer for a cube game (i.e., one line of input)."
  (let* ((tokens-pattern "[0-9]+|,|;|red|blue|green")
	 (string-tokens (cl-ppcre:all-matches-as-strings tokens-pattern line)))
    (append (mapcar (lambda (x)
		      (trivia:match x
			("," 'COMMA)
			(";" 'SEMICOLON)
			("red" 'RED)
			("blue" 'BLUE)
			("green" 'GREEN)
			(_ (parse-integer x))))
		    string-tokens)
	    (list 'EOF))))

(defun get-cube-input ()
  "Read in the input rows as a list of strings."
  (let* ((directory (uiop:pathname-parent-directory-pathname (uiop:getcwd)))
	 (filepath (merge-pathnames #p"data/day2/input" directory)))
    (with-open-file (in filepath :direction :input)
      (loop for line = (read-line in nil nil)
	    while line
	    collect line))))

(defparameter *tokens* nil)

(defun get-cube-games ()
  "Convert input to list of lexed games."
  (mapcar (lambda (line)
	    (progn
	      (defparameter *tokens* (lex-cube-game line))
	      (parse-start)))
	  (get-cube-input)))


;;; top-down recursive-descent parser
(defun parse-start ()
  (let ((game (parse-game))
	(next (pop *tokens*)))
    (if (eq next 'EOF)
	game
	(error "Unexpected token: expected EOF, got ~A~%" next))))

(defun parse-game ()
  (let ((id (parse-id))
	(sets (parse-sets)))
    (make-cube-game :id id :sets sets)))

(defun parse-id ()
  (let ((id (pop *tokens*)))
    (if (numberp id)
	id
	(error "Unexpected token: expected number, got ~A~%" id))))

(defun parse-sets ()
  (let ((set (parse-set))
	(next (car *tokens*)))
    (if (eq next 'SEMICOLON)
	(progn
	  (pop *tokens*)
	  (cons set (parse-sets)))
	(list set))))

(defun parse-set ()
  (let ((draws (parse-draws))
	(set (make-cube-set)))
    (dolist (draw draws set)
      (trivia:match (cdr draw)
	('RED (setf (cube-set-reds set) (car draw)))
	('BLUE (setf (cube-set-blues set) (car draw)))
	('GREEN (setf (cube-set-greens set) (car draw)))))))

(defun parse-draws ()
  (let ((draw (parse-draw))
	(next (car *tokens*)))
    (if (eq next 'COMMA)
	(progn
	  (pop *tokens*)
	  (cons draw (parse-draws)))
	(list draw))))

(defun parse-draw ()
  (let ((count (parse-count))
	(color (parse-color)))
    (cons count color)))

(defun parse-count ()
  (let ((count (pop *tokens*)))
    (if (numberp count)
	count
	(error "Unexpected token: expected number, got ~A~%" count))))

(defun parse-color ()
  (let ((color (pop *tokens*)))
    (if (or (eq color 'RED)
	    (eq color 'BLUE)
	    (eq color 'GREEN))
	color
	(error "Unexpected token: expected RED, BLUE, or GREEN, got ~A~%" color))))
;;; parser end

(defun possible-game-p (game)
  "Returns t if game is possible, nil otherwise."
  ;; these magic numbers come from the problem description
  (let ((max-red 12)
	(max-green 13)
	(max-blue 14)
	(sets (cube-game-sets game)))
    (every (lambda (set)
	     (and (<= (cube-set-reds set) max-red)
		  (<= (cube-set-blues set) max-blue)
		  (<= (cube-set-greens set) max-green)))
	   sets)))

(defun min-cube-set-power (game)
  "Returns the power of the minimum set of a game."
  (let ((min-reds (reduce #'max (mapcar (lambda (g)
					  (cube-set-reds g))
					(cube-game-sets game))))
	(min-greens (reduce #'max (mapcar (lambda (g)
					    (cube-set-greens g))
					  (cube-game-sets game))))
	(min-blues (reduce #'max (mapcar (lambda (g)
					   (cube-set-blues g))
					 (cube-game-sets game)))))
    (* min-reds min-greens min-blues)))

(defun solution-part1 ()
  (let* ((games (get-cube-games))
	 (possible-games (remove-if-not #'possible-game-p games))
	 (ids (mapcar (lambda (game)
			(cube-game-id game))
		      possible-games)))
    (reduce #'+ ids)))

(defun solution-part2 ()
  (reduce #'+ (mapcar #'min-cube-set-power (get-cube-games))))
