(in-package :day7)

(defconstant +jack+ 11)
(defconstant +queen+ 12)
(defconstant +king+ 13)
(defconstant +ace+ 14)
(defconstant +joker+ 1)

(defstruct hand
  (cards nil)
  (type nil)
  (bid nil))

(defun score-hand-vector (hand)
  "Return the score (type) of a vector of cards."
  (let ((counts (map 'vector (lambda (x)
			       (if (= x +joker+)
				   0
				   (count x hand)))
		     hand))
	(joker-count (count +joker+ hand)))
    (let ((type (cond ((some (lambda (x) (= x 5)) counts) :five-of-a-kind)
		      ((some (lambda (x) (= x 4)) counts) :four-of-a-kind)
		      ((and (some (lambda (x) (= x 3)) counts)
			    (some (lambda (x) (= x 2)) counts))
		       :full-house)
		      ((some (lambda (x) (= x 3)) counts) :three-of-a-kind)
		      ((= (count 2 counts) 4) :two-pair)
		      ((some (lambda (x) (= x 2)) counts) :one-pair)
		      (t :high-card))))
      ;; promote the hand by best use of jokers
      (dotimes (i joker-count)
	(case type
	  (:high-card (setf type :one-pair))
	  (:one-pair (setf type :three-of-a-kind))
	  (:two-pair (setf type :full-house))
	  (:full-house (setf type :four-of-a-kind))
	  (:three-of-a-kind (setf type :four-of-a-kind))
	  (:four-of-a-kind (setf type :five-of-a-kind))))
      type)))

(defun create-hand (value-pair)
  "Create a hand from the value-pair parsed from a line of input."
  (make-hand :cards (car value-pair)
	     :type (score-hand-vector (car value-pair))
	     :bid (cadr value-pair)))

(defun hand-type-value (hand)
  "Return the value of a hand (based on score/type)."
  (case (hand-type hand)
    (:high-card 1)
    (:one-pair 2)
    (:two-pair 3)
    (:three-of-a-kind 4)
    (:full-house 5)
    (:four-of-a-kind 6)
    (:five-of-a-kind 7)))

(defun cards-lessp (cards-1 cards-2)
  "Return t if cards-1 is less than cards-2 (score/type is not considered), nil otherwise."
  (dotimes (i (length cards-1))
    (unless (= (aref cards-1 i) (aref cards-2 i))
      (return (< (aref cards-1 i) (aref cards-2 i))))
    nil))

(defun hand-lessp (hand-1 hand-2)
  "Return t if hand-1 is of lesser rank than hand-2, nil otherwise."
  (if (eq (hand-type hand-1) (hand-type hand-2))
      (cards-lessp (hand-cards hand-1) (hand-cards hand-2))
      (< (hand-type-value hand-1) (hand-type-value hand-2))))

(defun read-input ()
  "Get the contents of the input as a string."
  (let ((filepath (merge-pathnames
		   #p"data/day7/input"
		   (uiop:pathname-parent-directory-pathname (uiop:getcwd)))))
    (with-open-file (file filepath :direction :input)
      (uiop:read-file-string file))))

(defun string-hand-to-vector (string)
  "Convert hand string to an integer vector that represents the hand."
  (if (not (= (length string) 5))
      (error "Unexpected hand length: expected length 5, got length ~A~%" (length string))
      (map 'vector (lambda (c)
		     (case c
		       (#\A +ace+)
		       (#\K +king+)
		       (#\Q +queen+)
		       (#\J +jack+)
		       (#\T 10)
		       (#\9 9)
		       (#\8 8)
		       (#\7 7)
		       (#\6 6)
		       (#\5 5)
		       (#\4 4)
		       (#\3 3)
		       (#\2 2)
		       (_ (error "Unexpected character in hand: ~A~%" c))))
	   string)))

(defun lex-input-and-parse (string)
  "Lex and parse the input into pairs of hand (int vector) and bid (integer)."
  (let ((string-token-pairs (mapcar #'str:words (str:lines string))))
    (mapcar (lambda (x)
	      (list (string-hand-to-vector (car x))
		    (parse-integer (cadr x))))
	    string-token-pairs)))

(defun get-hands ()
  "Return the vector of hands that represents the input."
  (map 'vector #'create-hand (lex-input-and-parse (read-input))))

(defun solution-part1 ()
  "Return the solution to part 1."
  (let ((hands (stable-sort (get-hands) #'hand-lessp))
	(winnings 0))
    (dotimes (i (length hands))
      (setf winnings (+ winnings (* (hand-bid (aref hands i)) (1+ i)))))
    winnings))

;;; part 2

(defun string-hand-to-vector-part2 (string)
  "Convert hand string to an integer vector that represents the hand, using the rules from part 2."
  (if (not (= (length string) 5))
      (error "Unexpected hand length: expected length 5, got length ~A~%" (length string))
      (map 'vector (lambda (c)
		     (case c
		       (#\A +ace+)
		       (#\K +king+)
		       (#\Q +queen+)
		       (#\J +joker+)
		       (#\T 10)
		       (#\9 9)
		       (#\8 8)
		       (#\7 7)
		       (#\6 6)
		       (#\5 5)
		       (#\4 4)
		       (#\3 3)
		       (#\2 2)
		       (otherwise (error "Unexpected character in hand: ~A~%" c))))
	   string)))

(defun lex-input-and-parse-part2 (string)
  "Lex and parse the input into pairs of hand (int vector) and bid (integer), using the rules from part 2."
  (let ((string-token-pairs (mapcar #'str:words (str:lines string))))
    (mapcar (lambda (x)
	      (list (string-hand-to-vector-part2 (car x))
		    (parse-integer (cadr x))))
	    string-token-pairs)))

(defun get-hands-part2 ()
  "Return the vector of hands representing the input, using the rules from part 2."
  (map 'vector #'create-hand (lex-input-and-parse-part2 (read-input))))

(defun solution-part2 ()
  "Return the solution to part 2."
  (let ((hands (stable-sort (get-hands-part2) #'hand-lessp))
	(winnings 0))
    (dotimes (i (length hands))
      (setf winnings (+ winnings (* (hand-bid (aref hands i)) (1+ i)))))
    winnings))
