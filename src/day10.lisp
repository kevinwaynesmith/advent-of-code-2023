(in-package :day10)

(defstruct coord
  (x nil)
  (y nil))

(defun read-input ()
  "Read in the input rows as a list of strings."
  (let* ((directory (uiop:pathname-parent-directory-pathname (uiop:getcwd)))
	 (filepath (merge-pathnames #p"data/day10/input" directory)))
    (with-open-file (in filepath :direction :input)
      (loop for line = (read-line in nil nil)
	    while line
	    collect line))))

(defparameter *x-dim* nil)
(defparameter *y-dim* nil)
(defparameter *diagram* nil)

(defun read-diagram ()
  "Read the input diagram and store it as a 2D array."
  (let ((lines (read-input)))
    (defparameter *x-dim* (length lines))
    (defparameter *y-dim* (length (car lines)))
    (defparameter *diagram* (make-array (list *x-dim* *y-dim*)
					:initial-contents lines))))

(defun find-start ()
  "Return the start coordinate of the diagram."
  (loop for i below *x-dim* do
    (loop for j below *y-dim* do
      (when (eq (aref *diagram* i j) #\S)
	(return-from find-start (make-coord :x i :y j))))))

(defun valid (coord)
  "Return t if the coord is in-bounds on the diagram, nil otherwise."
  (and (>= (coord-x coord) 0)
       (< (coord-x coord) *x-dim*)
       (>= (coord-y coord) 0)
       (< (coord-y coord) *y-dim*)))

(defun pipe-exits (pipe)
  "Return the (two) exit coordinates of a pipe."
  (let ((maybe-exits
	  (ecase (aref *diagram* (coord-x pipe) (coord-y pipe))
	    (#\| (list (make-coord :x (1- (coord-x pipe)) :y (coord-y pipe))
		       (make-coord :x (1+ (coord-x pipe)) :y (coord-y pipe))))
	    (#\- (list (make-coord :x (coord-x pipe) :y (1- (coord-y pipe)))
		       (make-coord :x (coord-x pipe) :y (1+ (coord-y pipe)))))
	    (#\L (list (make-coord :x (1- (coord-x pipe)) :y (coord-y pipe))
		       (make-coord :x (coord-x pipe) :y (1+ (coord-y pipe)))))
	    (#\J (list (make-coord :x (1- (coord-x pipe)) :y (coord-y pipe))
		       (make-coord :x (coord-x pipe) :y (1- (coord-y pipe)))))
	    (#\7 (list (make-coord :x (coord-x pipe) :y (1- (coord-y pipe)))
		       (make-coord :x (1+ (coord-x pipe)) :y (coord-y pipe))))
	    (#\F (list (make-coord :x (coord-x pipe) :y (1+ (coord-y pipe)))
		       (make-coord :x (1+ (coord-x pipe)) :y (coord-y pipe)))))))
    (remove-if-not #'valid maybe-exits)))

(defun possible-start-exits (start-coord)
  "Return the possible transitions from the start coordinate."
  (let* ((startx (coord-x start-coord))
	 (starty (coord-y start-coord))
	 (unfiltered (remove-if-not
		      #'valid
		      (list (make-coord :x (1- startx) :y (1- starty))
			    (make-coord :x (1- startx) :y starty)
			    (make-coord :x (1- startx) :y (1+ starty))
			    (make-coord :x startx :y (1- starty))
			    (make-coord :x startx :y (1+ starty))
			    (make-coord :x (1+ startx) :y (1- starty))
			    (make-coord :x (1+ startx) :y starty)
			    (make-coord :x (1+ startx) :y (1+ starty))))))
    (remove-if-not (lambda (x)
		     (member start-coord (pipe-exits x) :test #'equalp))
		   unfiltered)))

(defun follow-pipe (pipe entrance)
  "Return the result of following the pipe from the entrance coordinate."
  (car (remove-if (lambda (x)
		    (equalp x entrance))
		  (pipe-exits pipe))))

(defun get-loop ()
  (let ((start (find-start)))
    (do ((coords-traversed (list start) (cons current-location coords-traversed))
	 (current-location (car (possible-start-exits start))
			   (follow-pipe current-location (car coords-traversed))))
	((equalp current-location start) coords-traversed))))

(defun get-loop-length ()
  "Return the length of the loop."
  (let ((start (find-start)))
    (do ((current-location (car (possible-start-exits start))
			   (follow-pipe current-location last-location))
	 (last-location start current-location)
	 (steps 1 (1+ steps)))
	((equalp current-location start) steps))))

(defun solution-part1 ()
  "Return the solution to part 1."
  (read-diagram)
  (/ (get-loop-length) 2))

;;; part 2

(defun get-loop-hashtable ()
  "Return a hashtable which has keys for exactly the coordinates that are in the loop."
  (let* ((start (find-start))
	 (coords-table (al:alist-hash-table (list (cons start t)) :test #'equalp)))
    (do ((current-location (car (possible-start-exits start))
			   (follow-pipe current-location last-location))
	 (last-location start current-location))
	((equalp current-location start) coords-table)
      (setf (gethash current-location coords-table) t))))

(defun get-start-type (start)
  "Return the pipe type of the start coordinate."
  (let* ((exits (possible-start-exits start))
	 (first (car exits))
	 (second (cadr exits))
	 (above (1- (coord-x start)))
	 (below (1+ (coord-x start)))
	 (left (1- (coord-y start)))
	 (right (1+ (coord-y start))))
    (cond ((or (and (= (coord-x first) above)
		    (= (coord-x second) below))
	       (and (= (coord-x first) below)
		    (= (coord-x second) above)))
	   #\|)
	  ((or (and (= (coord-y first) left)
		    (= (coord-y second) right))
	       (and (= (coord-y first) right)
		    (= (coord-y second) left)))
	   #\-)
	  ((or (and (= (coord-x first) above)
		    (= (coord-y second) right))
	       (and (= (coord-y first) right)
		    (= (coord-x second) above)))
	   #\L)
	  ((or (and (= (coord-x first) below)
		    (= (coord-y second) right))
	       (and (= (coord-y first) right)
		    (= (coord-x second) below)))
	   #\F)
	  ((or (and (= (coord-x first) below)
		    (= (coord-y second) left))
	       (and (= (coord-y first) left)
		    (= (coord-x second) below)))
	   #\7)
	  ((or (and (= (coord-x first) above)
		    (= (coord-y second) left))
	       (and (= (coord-y first) left)
		    (= (coord-x second) above)))
	   #\J))))

(defun tile-type (coord)
  "Return the tile type at the given coordinate."
  (ecase (aref *diagram* (coord-x coord) (coord-y coord))
    (#\| #\|)
    (#\- #\-)
    (#\J #\J)
    (#\F #\F)
    (#\7 #\7)
    (#\L #\L)
    (#\. #\.)
    (#\S (get-start-type coord))))


(defun scanline-count-enclosed (coords-table)
  "Use scanline rendering (https://en.wikipedia.org/wiki/Scanline_rendering) to compute the number of tiles enclosed by the loop encoded in the input hashtable."
  (let ((in-region nil)
	(last-corner nil)
	(enclosed-tile-count 0))
    (loop for i below *x-dim* do
      ;; reset at start of each line
      (setf in-region nil)
      (setf last-corner nil)
      (loop for j below *y-dim* do
	(let ((current-coord (make-coord :x i :y j)))
	  (if (gethash current-coord coords-table) ; tile is in loop
	      (ecase (tile-type current-coord)
		(break)
		(#\| (setf in-region (not in-region))) ; border has been crossed
		(#\- ) ; horizontal pipe, so do nothing
		(#\J (ecase last-corner
		       ;; no relevant last corner has preceded the current corner
		       ((nil)
			(setf last-corner #\J))
		       (#\F
			;; the corners form a border, so we have swapped regions
			(setf in-region (not in-region))
			(setf last-corner nil))
		       (#\L
			;; the corners form a U, so no border has been crossed
			(setf last-corner nil))))
		(#\7 (ecase last-corner
		       ((nil)
			(setf last-corner #\7))
		       (#\L
			(setf in-region (not in-region))
			(setf last-corner nil))
		       (#\F
			(setf last-corner nil))))
		(#\L (ecase last-corner
		       ((nil)
			(setf last-corner #\L))))
		(#\F (ecase last-corner
		       ((nil)
			(setf last-corner #\F)))))
	      ;; tile is outside the loop
	      (when in-region
		(incf enclosed-tile-count))))))
    enclosed-tile-count))

(defun solution-part2 ()
  "Return the solution to part 2."
  (read-diagram)
  (scanline-count-enclosed (get-loop-hashtable)))
