(in-package :day1)

(defun get-value-part1 (str)
  "Get the integer value of a single row of the input, according to the part 1 rules."
  (let* ((pattern "[0-9]")
	 (fst (cl-ppcre:scan-to-strings pattern str))
	 (snd (cl-ppcre:scan-to-strings pattern (reverse str))))
    (parse-integer (str:concat fst snd))))

(defun get-value-part2 (str)
  "Get the integer value of a single row of the input, according to the part 2 rules."
  (let* ((pattern "[0-9]|one|two|three|four|five|six|seven|eight|nine")
	 (reverse-pattern "[0-9]|eno|owt|eerht|ruof|evif|xis|neves|thgie|enin")
	 (first-match (cl-ppcre:scan-to-strings pattern str))
	 (last-match (reverse (cl-ppcre:scan-to-strings reverse-pattern (reverse str))))
	 (fst (longnum-to-digit first-match))
	 (snd (longnum-to-digit last-match)))
    (parse-integer (str:concat fst snd))))

(defun longnum-to-digit (num-str)
  "Convert longform num to digit string, or return the input if it is a digit string."
  (trivia:match num-str
    ("one" "1")
    ("two" "2")
    ("three" "3")
    ("four" "4")
    ("five" "5")
    ("six" "6")
    ("seven" "7")
    ("eight" "8")
    ("nine" "9")
    (_ num-str)))

(defun read-input ()
  "Read in the input rows as a list of strings."
  (let* ((directory (uiop:pathname-parent-directory-pathname (uiop:getcwd)))
	 (filepath (merge-pathnames #p"data/day1/input" directory)))
    (with-open-file (in filepath :direction :input)
      (loop for line = (read-line in nil nil)
	    while line
	    collect line))))

(defun solution-part1 ()
  "Calculate the solution to part 1."
  (reduce #'+ (mapcar #'get-value-part1 (read-input))))

(defun solution-part2 ()
  "Calculate the solution to part 2."
  (reduce #'+ (mapcar #'get-value-part2 (read-input))))
