(in-package :day11)

(defstruct coord
  (:x nil)
  (:y nil))

(defun read-input ()
  "Read in the input rows as a list of strings."
  (let* ((directory (uiop:pathname-parent-directory-pathname (uiop:getcwd)))
	 (filepath (merge-pathnames #p"data/day11/input" directory)))
    (with-open-file (in filepath :direction :input)
      (loop for line = (read-line in nil nil)
	    while line
	    collect line))))

(defun lines-to-image (lines)
  "Convert image from list of strings to 2D array."
  (make-array (list (length lines) (length (car lines)))
	      :initial-contents lines))

(defun find-empty-dimensions (image)
  "Return the indexes of empty rows and empty galaxies."
  (let ((empty-rows (loop for i below (array-dimension image 0)
			  when (every (lambda (x)
					(eq x #\.))
				      (select:select image i t))
			    collect i))
	(empty-columns (loop for j below (array-dimension image 1)
			     when (every (lambda (x)
					   (eq x #\.))
					 (select:select image t j))
			       collect j)))
    (values empty-rows empty-columns)))

(defun stretch (index expanded-spaces &optional (expansion 2))
  "Return the 'stretched' index that results from expanding the empty regions to the expansion specified in the problem description."
  (+ index
     (* (1- expansion) (length (remove-if-not (lambda (x)
						(< x index))
					      expanded-spaces)))))

(defun manhattan-distance (coord1 coord2)
  "Return the manhattan distance between two coordinates."
  (+ (abs (- (coord-x coord1) (coord-x coord2)))
     (abs (- (coord-y coord1) (coord-y coord2)))))

(defun find-galaxies (image)
  "Return list of coordinates that contain galaxies."
  (al:flatten
   (loop for i below (array-dimension image 0)
	 collect (loop for j below (array-dimension image 1)
		       when (eq (aref image i j) #\#) collect (make-coord :x i :y j)))))

(defun solution-part1 ()
  "Return the solution to part 1."
  (let ((image (lines-to-image (read-input))))
    (multiple-value-bind (expanded-rows expanded-cols) (find-empty-dimensions image)
      (let ((galaxies
	      (mapcar
	       (lambda (coord)
		 (make-coord :x (stretch (coord-x coord) expanded-rows)
			     :y (stretch (coord-y coord) expanded-cols)))
	       (find-galaxies image))))
	(do ((rest (cdr galaxies) (cdr rest))
	     (first (car galaxies) (car rest))
	     (acc 0 (+ acc
		       (reduce #'+ (mapcar (lambda (x)
					     (manhattan-distance first x))
					   rest)))))
	    ((null rest) acc))))))

;;; part 2

;; same as part 1, but with a larger expansion of empty rows and columns
(defun solution-part2 ()
  "Return the solution to part 2."
  (let ((image (lines-to-image (read-input))))
    (multiple-value-bind (expanded-rows expanded-cols) (find-empty-dimensions image)
      (let ((galaxies
	      (mapcar
	       (lambda (coord)
		 (make-coord :x (stretch (coord-x coord) expanded-rows 1000000)
			     :y (stretch (coord-y coord) expanded-cols 1000000)))
	       (find-galaxies image))))
	(do ((rest (cdr galaxies) (cdr rest))
	     (first (car galaxies) (car rest))
	     (acc 0 (+ acc
		       (reduce #'+ (mapcar (lambda (x)
					     (manhattan-distance first x))
					   rest)))))
	    ((null rest) acc))))))
