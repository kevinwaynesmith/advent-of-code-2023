(in-package :day8)

(defun read-input ()
  "Read in the input rows as a list of strings."
  (let* ((directory (uiop:pathname-parent-directory-pathname (uiop:getcwd)))
	 (filepath (merge-pathnames #p"data/day8/input" directory)))
    (with-open-file (in filepath :direction :input)
      (loop for line = (read-line in nil nil)
	    while line
	    collect line))))

(defun lex-network-line (string)
  "Return tokens for a line of network input."
  (let ((string-tokens (cl-ppcre:all-matches-as-strings "[A-Z0-9]{3}" string)))
    (mapcar (lambda (x)
	      (intern x "KEYWORD"))
	    string-tokens)))

(defun parse-input (lines)
  "Parse the entire network into a directions string and network hash table."
  (let* ((directions (car lines))
	 (network-mappings (mapcar #'lex-network-line (cddr lines)))
	 (network (make-hash-table)))
    (dolist (mapping network-mappings nil)
      (setf (gethash (car mapping) network)
	    (list (cadr mapping) (caddr mapping))))
    (values directions network)))

(defun follow-directions (directions network)
  "Return the number of steps required to traverse from the start to the end."
  (do* ((current-location :AAA (case current-direction
				 (#\L (car (gethash current-location network)))
				 (#\R (cadr (gethash current-location network)))
				 (t (error "Unreachable"))))
	(steps-taken 0 (1+ steps-taken))
	(current-direction (aref directions 0)
			   (aref directions (mod steps-taken (length directions)))))
       ((eq current-location :ZZZ) steps-taken)))

(defun solution-part1 ()
  "Return the solution for part 1."
  (multiple-value-bind (directions network) (parse-input (read-input))
    (follow-directions directions network)))

;;; part 2

(defun starting-locations (hash-table)
  "Return the multiple starting locations, according to the part 2 rules."
  (remove-if-not (lambda (x)
		   (string= (str:s-last (symbol-name x))
			    "A"))
		 (al:hash-table-keys hash-table)))

(defun ending-locationsp (locations)
  "Return t if all locations are ending locations, nil otherwise."
  (every (lambda (x)
	   (string= (str:s-last (symbol-name x))
		    "Z"))
	 locations))

(defun follow-directions-part2-brute-force (directions network)
  "Return the number of steps required before every starting location ends in an ending location, calculated via brute force."
  (do* ((current-locations (starting-locations network)
			   (mapcar (lambda (loc)
				     (case current-direction
				       (#\L (car (gethash loc network)))
				       (#\R (cadr (gethash loc network)))
				       (t (error "Unreachable"))))
				   current-locations))
	(steps-taken 0 (1+ steps-taken))
	(current-direction (aref directions 0)
			   (aref directions (mod steps-taken (length directions)))))
       ((ending-locationsp current-locations) steps-taken)))

;; this is way too slow
(defun solution-part2-brute-force ()
  "Return the solution to part 2 via brute force."
  (multiple-value-bind (directions network) (parse-input (read-input))
    (follow-directions-part2-brute-force directions network)))

;; This magic solution relies on the fact that all the start nodes in the input
;; happen to require the same number of steps to reach the end node as their
;; end nodes take to reach themselves again. This allows us to reach the answer
;; by taking the LCM of the set of steps required for each starting location to
;; reach an ending location (incidentally, the ending locations all happen to be
;; unique).
(defun follow-directions-part2 (directions network start)
  "Return the number of steps required to reach an ending location from the given start location, according to the part 2 rules."
  (do* ((current-location start (case current-direction
				  (#\L (car (gethash current-location network)))
				  (#\R (cadr (gethash current-location network)))
				  (t (error "Unreachable"))))
	(steps-taken 0 (1+ steps-taken))
	(current-direction (aref directions 0)
			   (aref directions (mod steps-taken (length directions)))))
       ((string= (str:s-last (symbol-name current-location)) "Z") steps-taken)))

(defun solution-part2 ()
  "Return the solution to part 2."
  (multiple-value-bind (directions network) (parse-input (read-input))
    (apply #'lcm (mapcar (lambda (start)
			   (follow-directions-part2 directions network start))
			 (starting-locations network)))))
