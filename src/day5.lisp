(in-package :day5)

;;; part 1

(defstruct seed-problem
  (seeds nil)
  (mappings nil))

(defstruct mappings
  (seed-to-soil nil)
  (soil-to-fert nil)
  (fert-to-water nil)
  (water-to-light nil)
  (light-to-temp nil)
  (temp-to-hum nil)
  (hum-to-loc nil))

(defstruct mapping
  (src nil)
  (dst nil)
  (range nil))

(defun read-input ()
  "Get the contents of the input as a string."
  (let ((filepath (merge-pathnames
		   #p"data/day5/input"
		   (uiop:pathname-parent-directory-pathname (uiop:getcwd)))))
    (with-open-file (file filepath :direction :input)
      (uiop:read-file-string file))))


(defun lex-input (string)
  "Convert input string to list of tokens."
  (let ((string-tokens (cl-ppcre:all-matches-as-strings "[0-9]+|seeds|seed-to-soil|soil-to-fertilizer|fertilizer-to-water|water-to-light|light-to-temperature|temperature-to-humidity|humidity-to-location" string)))
    (flet ((string-token-to-token (string-token)
	     (trivia:match string-token
	       ("seeds" 'seeds)
	       ("seed-to-soil" 'seed-to-soil)
	       ("soil-to-fertilizer" 'soil-to-fertilizer)
	       ("fertilizer-to-water" 'fertilizer-to-water)
	       ("water-to-light" 'water-to-light)
	       ("light-to-temperature" 'light-to-temperature)
	       ("temperature-to-humidity" 'temperature-to-humidity)
	       ("humidity-to-location" 'humidity-to-location)
	       (otherwise (parse-integer string-token)))))
      (append (mapcar #'string-token-to-token string-tokens) (list 'eof)))))

(defparameter *tokens* nil)

;;; begin recursive-descent parser

(defun parse-start ()
  (let ((problem (parse-problem))
	(next (pop *tokens*)))
    (if (eq next 'eof)
	problem
	(error "Unexpected token: expected 'eof, got ~A~%" next))))

(defun parse-problem ()
  (let ((seeds (parse-seeds))
	(mappings (parse-mappings)))
    (make-seed-problem :seeds seeds
		       :mappings mappings)))

(defun parse-seeds ()
  (let ((next (pop *tokens*)))
    (if (eq next 'seeds)
	(parse-seeds-prime)
	(error "Unexpected token: expected 'seeds, got ~A~%" next))))

(defun parse-seeds-prime ()
  (if (symbolp (car *tokens*))
      nil
      (cons (parse-num) (parse-seeds-prime))))

(defun parse-num ()
  (let ((num (pop *tokens*)))
    (if (numberp num)
	num
	(error "Unexpected token: expected number, got ~A~%" num))))

(defun parse-mappings ()
  (make-mappings :seed-to-soil (parse-seed-to-soil)
		 :soil-to-fert (parse-soil-to-fert)
		 :fert-to-water (parse-fert-to-water)
		 :water-to-light (parse-water-to-light)
		 :light-to-temp (parse-light-to-temp)
		 :temp-to-hum (parse-temp-to-hum)
		 :hum-to-loc (parse-hum-to-loc)))

(defun parse-seed-to-soil ()
  (let ((next (pop *tokens*)))
    (if (eq next 'seed-to-soil)
	(parse-mapping)
	(error "Unexpected token: expected 'seed-to-soil, got ~A~%" next))))

(defun parse-soil-to-fert ()
  (let ((next (pop *tokens*)))
    (if (eq next 'soil-to-fertilizer)
	(parse-mapping)
	(error "Unexpected token: expected 'soil-to-fertilizer, got ~A~%" next))))

(defun parse-fert-to-water ()
  (let ((next (pop *tokens*)))
    (if (eq next 'fertilizer-to-water)
	(parse-mapping)
	(error "Unexpected token: expected 'fertilizer-to-water, got ~A~%" next))))

(defun parse-water-to-light ()
  (let ((next (pop *tokens*)))
    (if (eq next 'water-to-light)
	(parse-mapping)
	(error "Unexpected token: expected 'water-to-light, got ~A~%" next))))

(defun parse-light-to-temp ()
  (let ((next (pop *tokens*)))
    (if (eq next 'light-to-temperature)
	(parse-mapping)
	(error "Unexpected token: expected 'light-to-temperature, got ~A~%" next))))

(defun parse-temp-to-hum ()
  (let ((next (pop *tokens*)))
    (if (eq next 'temperature-to-humidity)
	(parse-mapping)
	(error "Unexpected token: expected 'temperature-to-humidity, got ~A~%" next))))

(defun parse-hum-to-loc ()
  (let ((next (pop *tokens*)))
    (if (eq next 'humidity-to-location)
	(parse-mapping)
	(error "Unexpected token: expected 'humidity-to-location, got ~A~%" next))))

(defun parse-mapping ()
  (let ((next (car *tokens*)))
    (if (symbolp next)
	nil
	(cons (make-mapping :dst (parse-num)
			    :src (parse-num)
			    :range (parse-num))
	      (parse-mapping)))))

;;; end recursive-descent parser


(defun follow-mapping (mapping i)
  "Return the index that is mapped to i if it is present, nil otherwise."
  (let ((dst (mapping-dst mapping))
	(src (mapping-src mapping))
	(range (mapping-range mapping)))
    (if (and (>= i src) (<= i (+ src range)))
	(+ dst (- i src))
	nil)))


(defun follow-mappings (mappings i)
  "Return the index that results from i following the list of mappings."
  (dolist (mapping mappings i)
    (let ((mapping-result (follow-mapping mapping i)))
      (when mapping-result
	(return mapping-result)))
    i))

(defparameter *seed-problem* nil)

(defun seed-to-loc (seed)
  "Return the location that is mapped to the seed."
  (let ((mappings (seed-problem-mappings *seed-problem*)))
    (follow-mappings
     (mappings-hum-to-loc mappings)
     (follow-mappings
      (mappings-temp-to-hum mappings)
      (follow-mappings
       (mappings-light-to-temp mappings)
       (follow-mappings
	(mappings-water-to-light mappings)
	(follow-mappings
	 (mappings-fert-to-water mappings)
	 (follow-mappings
	  (mappings-soil-to-fert mappings)
	  (follow-mappings
	   (mappings-seed-to-soil mappings)
	   seed)))))))))

(defun solution-part1 ()
  "Return the solution to part 1."
  (defparameter *tokens* (lex-input (read-input)))
  (defparameter *seed-problem* (parse-start))
  (apply #'min
	 (mapcar #'seed-to-loc (seed-problem-seeds *seed-problem*))))

;;; part 2

(defstruct range
  (begin nil)
  (end nil))

(defstruct accum
  (rem nil)
  (mapped nil))

(defun foldl (fn init list)
  "Left fold for lists."
  (if (null list)
      init
      (foldl fn (funcall fn (car list) init) (cdr list))))

(defun seeds-to-ranges (seeds)
  "Convert list of seeds to ranges."
  (mapcar (lambda (x)
	    (make-range :begin (car x)
			:end (1- (+ (car x) (cadr x)))))
	  (loop for (start len) on seeds by #'cddr while len
		collect (list start len))))

(defun follow-mapping-part2 (mapping range)
  "Return the range that is mapped to the input range as well as the range(s) that are not defined by the mapping."
  (let ((map-begin (mapping-src mapping))
	(map-end (1- (+ (mapping-src mapping) (mapping-range mapping))))
	(offset (- (mapping-dst mapping) (mapping-src mapping))))
    (if (or (> (range-begin range) map-end) (< (range-end range) map-begin))
	(make-accum :rem (list range) :mapped nil)
	(let* ((overlap-begin (if (>= (range-begin range) map-begin)
				  (range-begin range)
				  map-begin))
	       (overlap-end (if (<= (range-end range) map-end)
				(range-end range)
				map-end))
	       (mapped (make-range :begin (+ overlap-begin offset)
				   :end (+ overlap-end offset)))
	       (left-rem (if (< (range-begin range) map-begin)
			     (make-range :begin (range-begin range)
					 :end (1- overlap-begin))
			     nil))
	       (right-rem (if (> (range-end range) map-end)
			      (make-range :begin (1+ overlap-end)
					  :end (range-end range))
			      nil)))
	  (cond ((and left-rem right-rem) (make-accum :rem (list left-rem right-rem)
						      :mapped (list mapped)))
		(left-rem (make-accum :rem (list left-rem)
				      :mapped (list mapped)))
		(right-rem (make-accum :rem (list right-rem)
				       :mapped (list mapped)))
		(t (make-accum :rem nil
			       :mapped (list mapped))))))))

(defun follow-mappings-acc (mapping acc)
  "Map a list of ranges through a single mapping."
  (do* ((orig-rem (accum-rem acc) (cdr orig-rem))
	(mapped (accum-mapped acc))
	(new-rem nil))
       ((null orig-rem) (make-accum :rem new-rem
				    :mapped mapped))
    (let ((result (follow-mapping-part2 mapping (car orig-rem))))
      (when (accum-rem result)
	(setf new-rem (append (accum-rem result) new-rem)))
      (when (accum-mapped result)
	(setf mapped (append (accum-mapped result) mapped))))))

(defun follow-mappings-part2 (mappings ranges)
  "Map a list of ranges through a list of mappings corresponding to a single mapping phase."
  (let ((acc (foldl #'follow-mappings-acc (make-accum :rem ranges
						      :mapped nil)
		    mappings)))
    (cond ((and (accum-rem acc) (accum-mapped acc) (append (accum-rem acc) (accum-mapped acc))))
	  ((accum-rem acc) (accum-rem acc))
	  ((accum-mapped acc) (accum-mapped acc))
	  (t nil))))

(defun seeds-to-locs ()
  "Return the ranges of locations mapped to the seed ranges."
  (let ((initial-ranges (seeds-to-ranges (seed-problem-seeds *seed-problem*))))
    (follow-mappings-part2
     (mappings-hum-to-loc (seed-problem-mappings *seed-problem*))
     (follow-mappings-part2
      (mappings-temp-to-hum (seed-problem-mappings *seed-problem*))
      (follow-mappings-part2
       (mappings-light-to-temp (seed-problem-mappings *seed-problem*))
       (follow-mappings-part2
	(mappings-water-to-light (seed-problem-mappings *seed-problem*))
	(follow-mappings-part2
	 (mappings-fert-to-water (seed-problem-mappings *seed-problem*))
	 (follow-mappings-part2
	  (mappings-soil-to-fert (seed-problem-mappings *seed-problem*))
	  (follow-mappings-part2
	   (mappings-seed-to-soil (seed-problem-mappings *seed-problem*))
	   initial-ranges)))))))))

(defun solution-part2 ()
  "Return solution for part 2."
  (defparameter *tokens* (lex-input (read-input)))
  (defparameter *seed-problem* (parse-start))
  (loop for range in (seeds-to-locs)
	minimize (range-begin range)))
