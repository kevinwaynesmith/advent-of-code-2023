(in-package :day3)

(defun get-schematic-string ()
  "Get the contents of the input as a string."
  (let ((filepath (merge-pathnames
		   #p"data/day3/input"
		   (uiop:pathname-parent-directory-pathname (uiop:getcwd)))))
    (with-open-file (file filepath :direction :input)
      (uiop:read-file-string file))))

(defparameter *schematic-string* (get-schematic-string))

(defparameter *schematic-row-length* (1+ (position #\Newline *schematic-string*)))

(defun matches-to-ranges (match-list)
  "Expand regex matches to lists of the indexes that comprise each match."
  (labels ((aux (acc rem)
	     (if (null rem)
		 (nreverse acc)
		 (let* ((start (car rem))
			(end (1- (cadr rem)))
			(range (loop for i from start to end collect i)))
		   (aux (cons range acc) (cddr rem))))))
    (aux nil match-list)))

(defun ranges-to-integers (range-list)
  "Convert list of index ranges to the integers they represent in the schematic string."
  (mapcar (lambda (r)
	    (parse-integer (subseq *schematic-string* (car r) (1+ (car (last r))))))
	  range-list))

(defun adjacent-cells (i)
  "Find the indexes of the cells adjacent to the input index."
  (let* ((row *schematic-row-length*)
	 (above-left (1- (- i row)))
	 (above (- i row))
	 (above-right (1+ (- i row)))
	 (left (1- i))
	 (right (1+ i))
	 (below-left (1- (+ i row)))
	 (below (+ i row))
	 (below-right (1+ (+ i row)))
	 (adjacents (list
		     above-left above above-right
		     left right
		     below-left below below-right)))
    ;; remove out-of-bounds indexes
    (remove-if (lambda (x)
		 (or (< x 0)
		     (>= x (length *schematic-string*))
		     (eq (aref *schematic-string* x) #\Newline)))
	       adjacents)))

(defun symbol-char-p (i)
  "Return T if the schematic char at i is a schematic symbol, nil otherwise."
  (let ((char (aref *schematic-string* i)))
    (not (or (eq char #\.)
	     (eq char #\Newline)
	     (digit-char-p char)))))

(defun part-number-p (range)
  "Return T if the index range is a part number, nil otherwise."
  (some (lambda (i)
	  (some #'symbol-char-p (adjacent-cells i)))
	range))

(defun solution-part1 ()
  "Return the solution for Day 3 Part 1."
  (let* ((matches (cl-ppcre:all-matches "[0-9]+"
					*schematic-string*))
	 (ranges (matches-to-ranges matches))
	 (relevant-ranges (remove-if-not #'part-number-p ranges))
	 (part-numbers (ranges-to-integers relevant-ranges)))
    (reduce #'+ part-numbers)))

(defun find-stars ()
  "Return list of indexes of all stars in the schematic."
  (let ((acc nil))
    (dotimes (i (length *schematic-string*) (nreverse acc))
      (when (eq (aref *schematic-string* i) #\*)
	(push i acc)))))

(defun find-adjacent-ints (i &optional (ranges nil))
  "Return list of integers adjacent to the input schematic index."
  (let* ((adjacent-digits (remove-if-not
			   (lambda (j)
			     (digit-char-p (aref *schematic-string* j)))
			   (adjacent-cells i)))
	 (ranges (if ranges
		     ranges
		     (matches-to-ranges (cl-ppcre:all-matches "[0-9]+"
							      *schematic-string*))))
	 (relevant-ranges (remove-if-not
			   (lambda (j)
			     (intersection j adjacent-digits))
			   ranges)))
    (remove-duplicates (ranges-to-integers relevant-ranges))))


(defun solution-part2 ()
  "Return the solution for Day 3 Part 2."
  (let* ((stars (find-stars))
	 (matches (cl-ppcre:all-matches "[0-9]+" *schematic-string*))
	 (ranges (matches-to-ranges matches))
	 (adjacent-ints (mapcar (lambda (i)
				  (find-adjacent-ints i ranges))
				stars))
	 (relevant-ints (remove-if-not (lambda (x)
					 (= (length x) 2))
				       adjacent-ints))
	 (products (mapcar (lambda (xs) (* (car xs) (cadr xs)))
			   relevant-ints)))
    (reduce #'+ products)))
