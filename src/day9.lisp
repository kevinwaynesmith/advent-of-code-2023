(in-package :day9)

(defun read-input ()
  "Read in the input rows as a list of strings."
  (let* ((directory (uiop:pathname-parent-directory-pathname (uiop:getcwd)))
	 (filepath (merge-pathnames #p"data/day9/input" directory)))
    (with-open-file (in filepath :direction :input)
      (loop for line = (read-line in nil nil)
	    while line
	    collect line))))

(defun lex-and-parse-line (line)
  "Lex and parse a line of the input."
  (let ((string-tokens (cl-ppcre:all-matches-as-strings "[0-9\\-]+" line)))
    (mapcar #'parse-integer string-tokens)))

(defun lex-and-parse-lines (lines)
  "Lex and parse all the lines of the input."
  (mapcar #'lex-and-parse-line lines))

(defun get-sequences ()
  "Fetch the input as a list of vectors."
  (mapcar (lambda (list)
	    (make-array (length list) :initial-contents list))
	  (lex-and-parse-lines (read-input))))

(defun expanded-sequences (vec)
  "Create the list of expanded sequences of the input vector."
  (labels ((aux (vec-prime acc)
	     (if (every (lambda (x)
			  (= x 0))
			vec-prime)
		 (cons vec-prime acc)
		 (aux (map 'vector (lambda (x y)
				     (- y x))
			   vec-prime
			   (subseq vec-prime 1))
		      (cons vec-prime acc)))))
    (aux vec nil)))

(defun predict-next-value (vec)
  "Return the predicted next value of the input vector."
  (reduce #'+
	  (expanded-sequences vec)
	  :key (lambda (x)
		 (aref x (1- (length x))))))

(defun solution-part1 ()
  "Return the solution to part 1."
  (reduce #'+ (mapcar #'predict-next-value (get-sequences))))

;;; part 2

(defun foldl (fn init list)
  "Left fold on lists."
  (if (null list) init
      (foldl fn (funcall fn init (car list)) (cdr list))))

(defun predict-previous-value (values)
  "Return the predicted previous value of the list of values."
  (foldl (lambda (acc val)
	   (- val acc))
	 0
	 (mapcar (lambda (x)
		   (aref x 0))
		 (expanded-sequences values))))

(defun solution-part2 ()
  "Return the solution for part 2."
  (reduce #'+ (mapcar #'predict-previous-value (get-sequences))))
