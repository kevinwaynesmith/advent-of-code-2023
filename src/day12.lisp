(in-package :day12)

(defun get-input ()
  "Get the contents of the input as a string."
  (let ((filepath (merge-pathnames
		   #p"data/day12/input"
		   (uiop:pathname-parent-directory-pathname (uiop:getcwd)))))
    (with-open-file (file filepath :direction :input)
      (uiop:read-file-string file))))

(defstruct row
  (conditions nil)
  (groups nil))

(defun lex-and-parse-line (line)
  "Convert a line of input to a row."
  (let ((tokens (cl-ppcre:all-matches-as-strings
		 "[#.?]+|[0-9]+"
		 line)))
    ;; consecutive .'s are replaced with a singular . for simplicity
    (make-row :conditions (cl-ppcre:regex-replace-all "\\.+" (car tokens) ".")
	      :groups (mapcar #'parse-integer (cdr tokens)))))

(defun string-to-rows (string)
  "Convert entire input to a list of rows."
  (mapcar #'lex-and-parse-line (str:lines string)))

;; from Paul Graham's On Lisp
(defun memoize (fn)
  "Return a closure that is the memoized version of the input function."
  (let ((cache (make-hash-table :test #'equal)))
    (lambda (&rest args)
      (multiple-value-bind
	    (value exists)
	  (gethash args cache)
	(if exists
	    value
	    (setf (gethash args cache)
		  (apply fn args)))))))

(defun arrangements (string groups)
  "Return the number of arrangements that can be made from the condition record (string) and list of sizes of contiguous damaged springs (groups)."
  (cond
    ;; all groups accounted for and no dangling #s, so this is a valid arrangement
    ((and (null groups)
	  (every (lambda (x)
		   (or (eq x #\.)
		       (eq x #\?)))
		 string))
     1)
    ;; there are dangling #s or unaccounted-for groups, so this is not a valid arrangement
    ((or (null groups) (str:emptyp string))
     0)
    ;; skip over leading .s
    ((str:starts-with-p "." string) (arrangements (str:substring 1 nil string) groups))
    (t
     ;; regex pattern matches condition record prefixes that match the
     ;; relevant contiguous damaged spring group
     (multiple-value-bind (start end)
	 (cl-ppcre:scan (str:concat "^[#?]{"
				    (write-to-string (car groups))
				    "}"
				    "([.?]|$)")
			string)
       (cond
	 ;; there is a match and string starts with ?, so this prefix may correspond
	 ;; to the relevant group if it is assigned # (first summand),
	 ;; but we also need to see what results from assigning it . (second summand)
	 ((and start (str:starts-with-p "?" string))
	  (+ (arrangements (str:substring end nil string) (cdr groups))
	     (arrangements (str:substring 1 nil string) groups)))
	 ;; there is a match and string starts with #, so this must be
	 ;; the beginning of the group if it is a valid arrangement
	 (start
	  (arrangements (str:substring end nil string) (cdr groups)))
	 ;; no match and string begins with ?, so we assign the ? to .
	 ((str:starts-with-p "?" string)
	  (arrangements (str:substring 1 nil string) groups))
	 ;; no match and string starts with #, so this is not a valid arrangement
	 (t 0))))))

;; to use a memoized version of #'arrangements, changing the definition of
;; #'arrangements to its memoized version is necessary because it is a recursive function
(setf (fdefinition 'arrangements) (memoize #'arrangements))

;;; The next three functions aren't used in the final iteration of
;;; this solution and are kept here only for posterity

;; relevant for the brute-force approach
(defun get-?s (string)
  "Return the indexes of all #\?s in a string."
  (loop for (a _) on (cl-ppcre:all-matches "\\?" string) by #'cddr
	collect a))

;; relevant for the brute force approach
(defun check (r)
  "Return t if r represents a valid arrangement, nil otherwise."
  (let ((actual-groups (loop for (a b) on (cl-ppcre:all-matches "#+" (row-conditions r)) by #'cddr
			     collect (- b a))))
    (equal actual-groups (row-groups r))))

;; This was a quick initial solution to solve part 1, but it is inefficient
;; and not friendly to memoization.
;; This generates each possible permutation (exponential in the number of ?s)
;; of arrangements and checks each one for validity to see how many valid
;; arrangements there are.
(defun arrangements-brute-force (r)
  "Use brute force to count the possible arrangements of the input row."
  (labels ((aux (indexes)
	     (cond ((null indexes) (if (check r) 1 0))
		   (t
		    (+ (progn
			 (setf (aref (row-conditions r) (car indexes)) #\.)
			 (aux (cdr indexes)))
		       (progn
			 (setf (aref (row-conditions r) (car indexes)) #\#)
			 (aux (cdr indexes))))))))
    (aux (get-?s (row-conditions r)))))

(defun solution-part1 ()
  "Return the solution to part 1."
  (reduce #'+ (mapcar (lambda (x)
			(arrangements (row-conditions x)
				      (row-groups x)))
		      (string-to-rows (get-input)))))

;;; part 2

(defun expand-row (r)
  "Return the expanded row represented by the input, according to the part 2 rules."
  (make-row :conditions (str:join "?" (make-list 5 :initial-element (row-conditions r)))
	    :groups (al:flatten (make-list 5 :initial-element (row-groups r)))))

(defun solution-part2 ()
  "Return the solution to part 2."
  (reduce #'+ (mapcar (lambda (x)
			(arrangements (row-conditions x)
				      (row-groups x)))
		      (mapcar #'expand-row (string-to-rows (get-input))))))
