(defpackage aoc2023
  (:use :cl))

(defpackage #:day1
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day2
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day3
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day4
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day5
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day6
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day7
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day8
  (:use :cl)
  (:local-nicknames (:al :alexandria))
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day9
  (:use :cl)
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day10
	    (:use :cl)
	    (:local-nicknames (:al :alexandria))
	    (:export #:solution-part1
		     #:solution-part2))

(defpackage #:day11
  (:use :cl)
  (:local-nicknames (:al :alexandria))
  (:export #:solution-part1
	   #:solution-part2))

(defpackage #:day12
  (:use :cl)
  (:local-nicknames (:al :alexandria))
  (:export #:solution-part1
	   #:solution-part2))
