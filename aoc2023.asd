(defsystem "aoc2023"
  :long-name "Advent of Code 2023"
  :version "0.0.1"
  :author "Kevin Wayne Smith"
  :maintainer "Kevin Wayne Smith"
  :license "MIT"
  :depends-on ("str" "cl-ppcre" "trivia" "select")
  :components ((:module "src"
		:components
		((:file "package")
		 (:file "day1")
		 (:file "day2")
		 (:file "day3")
		 (:file "day4")
		 (:file "day5")
		 (:file "day6")
		 (:file "day7")
		 (:file "day8")
		 (:file "day9")
		 (:file "day10")
		 (:file "day11")
		 (:file "day12")
		 (:file "main"))))
  :description "My solutions to Advent of Code 2023")
